package controller;

import java.sql.*;

public class DBConnect {
    private static Connection db;
    private static Statement stmt;

    static {
        try {
            db = new DBConnect().Connect();
            stmt = db.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getDb() {
        return db;
    }

    public static Statement getStmt() {
        return stmt;
    }

    public Connection Connect() {

        Connection conn = null;
        try {
            String url = "jdbc:mariadb://localhost/AFC";
            String user = "root";
            String password = "123456";
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
}