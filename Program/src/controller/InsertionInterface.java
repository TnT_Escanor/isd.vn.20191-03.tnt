package controller;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;
import hust.soict.se.recognizer.TicketRecognizer;

public class InsertionInterface {

	private static InsertionInterface instance = new InsertionInterface();

	public static InsertionInterface getInstance() {
		return instance;
	}
	
	public String processTicket(String barCode) throws InvalidIDException {
		TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
		return ticketRecognizer.process(barCode);
	}

	public String processCard(String barCode) throws InvalidIDException {

		CardScanner cardScanner = CardScanner.getInstance();
		return cardScanner.process(barCode);
	}
}