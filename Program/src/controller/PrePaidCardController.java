package controller;

import model.PrepaidCard;
import view.UI;

import java.sql.*;
import java.util.*;

public class PrepaidCardController extends PlatformController {
	private static PrepaidCardController instance = new PrepaidCardController();

	private List<PrepaidCard> listPrepaidCard;
	private Statement stmt = DBConnect.getStmt();
	private PrepaidCard prepaidCard;
	private UI ui = UI.getInstance();

	public static PrepaidCardController getInstance() {
		return instance;
	}

	public List<PrepaidCard> getListPrepaidCard() throws SQLException {
		listPrepaidCard = new ArrayList<PrepaidCard>();
		ResultSet rs = this.stmt.executeQuery("SELECT * FROM prepaid_card");
		while (rs.next()) {
			listPrepaidCard.add(new PrepaidCard(
					rs.getString(1),
					rs.getString(2),
					rs.getString(3),
					rs.getFloat(4),
					rs.getString(5),
					rs.getInt(6)
			));
		}
		return this.listPrepaidCard;
	}

	public boolean enterStation(int enterStationId, String code) throws SQLException {
		this.setPrepaidCard(code);

		if (this.prepaidCard.getStatus().equals("using")) {
			ui.setMessage("Invalid Prepaid Card");
			ui.setError("Prepaid Card is already in use");
			return false;
		}

		if (this.prepaidCard.getBalance() < 1.9f) {
			ui.setMessage("Invalid Prepaid Card");
			ui.setError("Not enough balance: Expected at least 1.9 euros");
			return false;
		}

		this.stmt.executeUpdate("UPDATE prepaid_card SET status='using', enter_station_id=" + enterStationId + " WHERE code='" + code + "'");
		ui.setMessage(null);
		ui.setError(null);
		return true;
	}

	public boolean exitStation(int exitStationId, String code) throws SQLException {
		this.setPrepaidCard(code);

		if (this.prepaidCard.getStatus().equals("ready")) {
			ui.setMessage("Invalid Prepaid Card");
			ui.setError("Prepaid Card is not used to enter a station before");
			return false;
		}

		Float expectedFee = this.calculateFee(this.prepaidCard.getEnterStationId(), exitStationId);
		if (this.prepaidCard.getBalance() < expectedFee) {
			ui.setMessage("Invalid Prepaid Card");
			ui.setError("Not enough balance: Expected " + expectedFee + " euros");
			return false;
		}

		this.stmt.executeUpdate("UPDATE prepaid_card SET status='ready', enter_station_id=null, balance=" + (this.prepaidCard.getBalance() - expectedFee) + " WHERE code='" + code + "'");
		ui.setMessage(null);
		ui.setError(null);
		ui.setInfo("Balance: " + (this.prepaidCard.getBalance() - expectedFee) + " euros");
		return true;
	}

	private void setPrepaidCard(String code) throws SQLException {
		ResultSet rs = this.stmt.executeQuery("SELECT * FROM prepaid_card WHERE code='" + code + "'");
		rs.next();
		this.prepaidCard = new PrepaidCard(
				rs.getString(1),
				rs.getString(2),
				rs.getString(3),
				rs.getFloat(4),
				rs.getString(5),
				rs.getInt(6)
		);
	}
}
