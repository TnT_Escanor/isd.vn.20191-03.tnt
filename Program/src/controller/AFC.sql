DROP SCHEMA IF EXISTS AFC;
CREATE SCHEMA AFC
COLLATE = utf8_general_ci;

USE AFC;

CREATE TABLE station
(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR (50) NULL,
    distance FLOAT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE ticket_one_way
(
    id VARCHAR(14) NOT NULL UNIQUE,
    code VARCHAR(16) NOT NULL UNIQUE,
    bar_code VARCHAR(8) NOT NULL UNIQUE,
    fare FLOAT NULL,
    status VARCHAR(7) NULL,
    embarkation_id INT NULL,
    disembarkation_id INT NULL,
    enter_station_id INT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (embarkation_id) REFERENCES station(id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (disembarkation_id) REFERENCES station(id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (enter_station_id) REFERENCES station(id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE ticket_24h
(
    id VARCHAR (14) NOT NULL UNIQUE,
    code VARCHAR(16) NOT NULL UNIQUE,
    bar_code VARCHAR(8) NOT NULL UNIQUE,
    expire_time DATETIME NULL,
    status VARCHAR(7) NULL,
    PRIMARY KEY (id)
);

CREATE TABLE prepaid_card
(
    id VARCHAR(14) NOT NULL UNIQUE,
    code VARCHAR(16) NOT NULL UNIQUE,
    bar_code VARCHAR(8) NOT NULL UNIQUE,
    balance FLOAT NULL,
    status VARCHAR(7) NULL,
    enter_station_id INT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (enter_station_id) REFERENCES station(id) ON DELETE SET NULL ON UPDATE CASCADE
);

INSERT INTO station
    (name, distance)
VALUES
    ('Saint-Lazare', 0),
    ('Madeleine', 5),
    ('Pyramides', 8.5),
    ('Chatelet', 11.3),
    ('Gare de Lyon', 15.8),
    ('Bercy', 18.9),
    ('Cour Saint-Emilion', 22),
    ('Bibliotheque Francois Mitterrand', 25.3),
    ('Olympiades', 28.8);


INSERT INTO ticket_one_way
(id, code, bar_code, fare, status, embarkation_id, disembarkation_id, enter_station_id)
VALUES ('OW201912121234', '047cb2a4b57c9eaa', 'zujrktld', 3.42, 'ready', 7, 9, NULL),
       ('OW201912125678', 'd804269fb0c6842a', 'urjtnkgy', 9.72, 'using', 2, 5, 4),
       ('OW201912129101', '05ef89e23d96c6c6', 'uxbqopur', 8.56, 'expired', 1, 6, 2);



INSERT INTO ticket_24h
    (id, code, bar_code, expire_time, status)
VALUES ('TF201912124839', '07c84c6c4ba59f88', 'ijklmnop', NULL, 'ready'),
       ('TF201912120245', '33d89a87ee9dba49', 'qozidjrr', '2019-12-13T19:42:00', 'ready'),
       ('TF201912120045', '78cacb5e983e0df3', 'ppovnuet', '2019-12-07T21:14:00', 'using'),
       ('TF201912121123', 'bab1246b02772bb0', 'ponmlkji', '2019-12-13T14:35:00', 'expired');


INSERT INTO prepaid_card
    (id, code, bar_code, balance, status, enter_station_id)
VALUES ('PC201912121937', 'c7c42c520059cd2f', 'JFKSMXRY', 12.83, 'ready', NULL),
       ('PC201912128493', '8bed5643c666c871', 'NORUQZVV', 5.65, 'using', 3);