package controller;

import model.Station;

import java.sql.*;
import java.util.*;

public class StationController {
    private static StationController instance = new StationController();

    private Statement stmt = DBConnect.getStmt();
    private List<Station> listStation;

    public static StationController getInstance() {
        return instance;
    }

    public List<Station> getListStation() throws SQLException {
        listStation = new ArrayList<Station>();
        ResultSet rs = this.stmt.executeQuery("SELECT * FROM station");
        while (rs.next()) {
            listStation.add(new Station(rs.getInt(1), rs.getString(2), rs.getFloat(3)));
        }
        return this.listStation;
    }

    public String getStationName(int id) throws SQLException {
        ResultSet rs = this.stmt.executeQuery("SELECT name FROM station WHERE id=" + id);
        rs.next();
        String name = rs.getString("name");
        return name;
    }

    public Float getDistance(int id) throws SQLException {
        ResultSet rs = this.stmt.executeQuery("SELECT distance FROM station WHERE id=" + id);
        rs.next();
        Float distance = rs.getFloat("distance");
        return distance;
    }
}