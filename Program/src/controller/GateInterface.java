package controller;
import hust.soict.se.gate.Gate;

public class GateInterface {

	private static GateInterface instance = new GateInterface();
	private Gate gate = Gate.getInstance();

	public static GateInterface getInstance() {
		return instance;
	}

	protected void openGate() {
		gate.open();
	}

	protected void closeGate() {
		gate.close();
	}
}