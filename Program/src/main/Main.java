package main;

import hust.soict.se.customexception.InvalidIDException;
import view.UI;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws InvalidIDException, SQLException {
        UI ui = UI.getInstance();
        ui.showMenu();
    }
}
