package model;

public class OneWayTicket {

	private String id;
	private String code;
	private String barCode;
	private Float fare;
	private String status;
	private int embarkStationId;
	private int disembarkStationId;
	private int enterStationId;

	public OneWayTicket(String id, String code, String barCode, Float fare, String status, int embarkStationId, int disembarkStationId, int enterStationId) {
		this.id = id;
		this.code = code;
		this.barCode = barCode;
		this.fare = fare;
		this.status = status;
		this.embarkStationId = embarkStationId;
		this.disembarkStationId = disembarkStationId;
		this.enterStationId = enterStationId;
	}

	public String getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getBarCode() {
		return barCode;
	}

	public Float getFare() {
		return fare;
	}

	public String getStatus() {
		return this.status;
	}

	public int getEmbarkStationId() {
		return embarkStationId;
	}

	public int getDisembarkStationId() {
		return disembarkStationId;
	}

	public int getEnterStationId() {
		return enterStationId;
	}
}