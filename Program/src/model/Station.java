package model;

public class Station {

	private int id;
	private String name;
	private float distance;

	public Station(int id, String name, float distance) {
		this.id = id;
		this.name = name;
		this.distance = distance;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public float getDistance() {
		return this.distance;
	}
}