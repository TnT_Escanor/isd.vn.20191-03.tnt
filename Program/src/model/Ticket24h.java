package model;

import java.sql.Timestamp;

public class Ticket24h {

	private String id;
	private String code;
	private String barCode;
	private Timestamp expireTime;
	private String status;

	public Ticket24h(String id, String code, String barCode, Timestamp expireTime, String status) {
		this.id = id;
		this.code = code;
		this.barCode = barCode;
		this.expireTime = expireTime;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getBarCode() {
		return barCode;
	}

	public Timestamp getExpireTime() {
		return expireTime;
	}

	public String getStatus() {
		return status;
	}
}