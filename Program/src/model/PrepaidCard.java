package model;

public class PrepaidCard {
	private String id;
	private String code;
	private String barCode;
	private Float balance;
	private String status;
	private int enterStationId;

	public PrepaidCard(String id, String code, String barCode, Float balance, String status, int enterStationId) {
		this.id = id;
		this.code = code;
		this.barCode = barCode;
		this.balance = balance;
		this.status = status;
		this.enterStationId = enterStationId;
	}

	public String getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getBarCode() {
		return barCode;
	}

	public Float getBalance() {
		return balance;
	}

	public String getStatus() {
		return status;
	}

	public int getEnterStationId() {
		return enterStationId;
	}
}